import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by it-school on 09.10.2017.
 */
public class Wait {
    final WebDriver driver;

    public Wait(WebDriver driver) {
        this.driver = driver;
    }

    void waitCookies (){
        WebDriverWait wait = new WebDriverWait(driver,60);
        wait.until((WebDriver web) -> driver.manage().getCookieNamed("aff")!=null);
    }
}
